import React, { createElement } from 'react';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import {gsap} from "gsap" 
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { RectAreaLightHelper } from 'three/examples/jsm/helpers/RectAreaLightHelper'
import { CSS2DRenderer, CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import * as THREE from "three";
import './App.scss';
import busteGLB from './3Dmodel/busteopti2.glb'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import textureSphere from "./textures/textureDodecahedronTEST.png"
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { text } from '@fortawesome/fontawesome-svg-core';
class App extends React.Component {

  constructor() {
    super();
    this.state = {
      width: window.innerWidth,
    };
  }


  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }
  
  // make sure to remove the listener
  // when the component is not mounted anymore
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }
  
  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  componentDidMount() {

    if (window.innerWidth >= 500) {

    var speed = 0.008
    var tagetList = []
    const nbImg = 5
    const imagesTab = [
      ["https://c4.wallpaperflare.com/wallpaper/600/919/630/digital-art-illustration-lofi-hd-wallpaper-preview.jpg", "https://wallpaperaccess.com/full/642865.jpg", "https://i.pinimg.com/originals/64/ed/d6/64edd6602a0772197f61915c99d20e85.jpg", "https://greepx.com/wp-content/uploads/2018/01/1515862796_541_lo-fi.jpg", "https://www.teahub.io/photos/full/198-1988812_lo-fi-wallpaper-4k.jpg"],
      ["https://c4.wallpaperflare.com/wallpaper/600/919/630/digital-art-illustration-lofi-hd-wallpaper-preview.jpg", "https://wallpaperaccess.com/full/642865.jpg", "https://i.pinimg.com/originals/64/ed/d6/64edd6602a0772197f61915c99d20e85.jpg", "https://greepx.com/wp-content/uploads/2018/01/1515862796_541_lo-fi.jpg", "https://www.teahub.io/photos/full/198-1988812_lo-fi-wallpaper-4k.jpg"],
      ["https://c4.wallpaperflare.com/wallpaper/600/919/630/digital-art-illustration-lofi-hd-wallpaper-preview.jpg", "https://wallpaperaccess.com/full/642865.jpg", "https://i.pinimg.com/originals/64/ed/d6/64edd6602a0772197f61915c99d20e85.jpg", "https://greepx.com/wp-content/uploads/2018/01/1515862796_541_lo-fi.jpg", "https://www.teahub.io/photos/full/198-1988812_lo-fi-wallpaper-4k.jpg"],
      ["https://c4.wallpaperflare.com/wallpaper/600/919/630/digital-art-illustration-lofi-hd-wallpaper-preview.jpg", "https://wallpaperaccess.com/full/642865.jpg", "https://i.pinimg.com/originals/64/ed/d6/64edd6602a0772197f61915c99d20e85.jpg", "https://greepx.com/wp-content/uploads/2018/01/1515862796_541_lo-fi.jpg", "https://www.teahub.io/photos/full/198-1988812_lo-fi-wallpaper-4k.jpg"],
      ["https://c4.wallpaperflare.com/wallpaper/600/919/630/digital-art-illustration-lofi-hd-wallpaper-preview.jpg", "https://wallpaperaccess.com/full/642865.jpg", "https://i.pinimg.com/originals/64/ed/d6/64edd6602a0772197f61915c99d20e85.jpg", "https://greepx.com/wp-content/uploads/2018/01/1515862796_541_lo-fi.jpg", "https://www.teahub.io/photos/full/198-1988812_lo-fi-wallpaper-4k.jpg"]
    ]

    const titleTab = [
      "Thot",
      "Spectra",
      "Ko-notes",
      "SuperMirror",
      " "
    ]
    const descTab = [
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      "Spectra est une application web qui devait répondre aux besoins d’une entreprise d’analyse de matériau qui cherchait à rendre plus accessibles leur base de plusieurs centaines de milliers d’analyses. Ce projet m’a permis d’apprendre à gérer un gros volume de tout données tout en le retransmettant le plus fidèlement et clairement possible.",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    ]
    const urlsTab = [
      "http://thot.boudinski.com",
      "http://spectra.boudinski.com",
      "https://ko-notes.herokuapp.com",
      "http://super-mirror.boudinski.com"
    ]

    class TextScramble {
      constructor(el) {
        this.el = el
        this.chars = '!<>-_\\/[]{}—=+*^?#________'
        this.update = this.update.bind(this)
      }
      setText(newText) {
        const oldText = this.el.innerText
        const length = newText.length
        const promise = new Promise((resolve) => this.resolve = resolve)
        this.queue = []
        for (let i = 0; i < length; i++) {
          const from = oldText[i] || ''
          const to = newText[i] || ''
          const start = Math.floor(Math.random() * 40)
          const end = start + Math.floor(Math.random() * 40)
          this.queue.push({ from, to, start, end })
        }
        cancelAnimationFrame(this.frameRequest)
        this.frame = 0
        this.update()
        return promise
      }
      update() {
        let output = ''
        let complete = 0
        for (let i = 0, n = this.queue.length; i < n; i++) {
          let { from, to, start, end, char } = this.queue[i]
          if (this.frame >= end) {
            complete++
            output += to
          } else if (this.frame >= start) {
            if (!char || Math.random() < 0.28) {
              char = this.randomChar()
              this.queue[i].char = char
            }
            output += `<span class="dud">${char}</span>`
          } else {
            output += from
          }
        }
        this.el.innerHTML = output
        if (complete === this.queue.length) {
          this.resolve()
        } else {
          this.frameRequest = requestAnimationFrame(this.update)
          this.frame += 0.6
        }
      }
      randomChar() {
        return this.chars[Math.floor(Math.random() * this.chars.length)]
      }
    }

    //scene
    var scene = new THREE.Scene();
    scene.background = new THREE.Color(0x0);
    //container
    const container = document.getElementById('container')
    //camera
    var camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 10000);
    camera.position.z = 12;
    camera.position.y = 1;
    //renderer
    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);

    document.addEventListener('mousedown', onDocumentMouseDown, false);

    //init text
    var labelRenderer = new CSS2DRenderer();
    labelRenderer.setSize( window.innerWidth, window.innerHeight );
    labelRenderer.domElement.style.position = 'absolute';
    labelRenderer.domElement.style.top = '0px';
    container.appendChild( labelRenderer.domElement )
    
    //init spheres
    var texts = []
    var objects = [];
    var material = new THREE.MeshStandardMaterial({ color: 0xFFFFFF });
    var geometry = new THREE.DodecahedronBufferGeometry(0.5, 0);

    const count = 5;
    var t_val = []
    for (let i = 0; i < count; i++) {

      const mesh = new THREE.Mesh(geometry, material);

      const t = i / count * 2 * Math.PI;
      t_val.push(t)
      mesh.position.x = Math.cos(t) * 3;
      mesh.position.y = 3
      mesh.position.z = Math.sin(t) * 3;
      scene.add(mesh);


      
      objects.push(mesh);
    }
    for (let i = 0; i < count; i++) {
    var textDiv = document.createElement( 'div' );
    var textLine = document.createElement( 'div' );
    var textP = document.createElement("p")
    // textP.textContent = titleTab[i];
    var el = textP
    var fx = new TextScramble(el)
    fx.setText(titleTab[i])
    textDiv.appendChild(textP)
    textDiv.appendChild(textLine)
    textDiv.setAttribute("id", "textDiv")
    textLine.setAttribute("id", "textLine")
    textP.setAttribute("id", "textP")
    var textLabel = new CSS2DObject( textDiv );
    textLabel.position.set( 0,2.5,0);
    scene.add( textLabel );
    texts.push(textLabel)

    }

    window.addEventListener('resize', function() {
      camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );
   });
    //init buste
    const busteMaterial = new THREE.MeshStandardMaterial({ roughness: 0.1, metalness: 0.5 });
    var theModel;
    const MODEL_PATH = busteGLB
    var loader = new GLTFLoader();

    loader.load(MODEL_PATH, function (gltf) {//loader

      theModel = gltf.scene;

      // Dimensions du modèle 3D   
      theModel.scale.set(.01, .01, .01);

      // Positionnement du modèle sur l'axe y  
      theModel.position.set(-0.2, 0, -0.5)
      theModel.traverse(child => {
        if (child.material) child.material = busteMaterial;
        //child.material.metalness = 0 //pour mettre texture original

      });
      // Ajoute le modèle à la scène
      scene.add(theModel);

    }, undefined, function (error) {
      console.error(error)
    });



    //init lights 

    var followLight = new THREE.SpotLight(0xffffff, 2.3, 20, 7, 1);
    followLight.position.set(0, 2, 17);

    scene.add(followLight)
    const AmbientLight = new THREE.AmbientLight(0xffffff, 0.2);
    scene.add(AmbientLight);


    var color = 0x00ff00
    var width = 0.05;
    var height = 1000;
    var intensity = 70
    //milieu
    const rectLight1 = new THREE.RectAreaLight(color, intensity, width, height);
    rectLight1.position.set(0, 15, 0);
    rectLight1.lookAt(0, 0, 0);
    scene.add(rectLight1)
    const rectLightHelper1 = new RectAreaLightHelper(rectLight1);
    rectLight1.add(rectLightHelper1);
    const rectLight2 = new THREE.RectAreaLight(color, intensity, width, height);
    rectLight2.position.set(0, -8, 0);
    rectLight2.lookAt(0, 0, 0);
    scene.add(rectLight2)
    const rectLightHelper2 = new RectAreaLightHelper(rectLight2);
    rectLight2.add(rectLightHelper2);
    // droit
    const rectLight3 = new THREE.RectAreaLight(color, intensity, width, height);
    rectLight3.position.set(10, 15, 0);
    rectLight3.lookAt(10, 0, 0);
    scene.add(rectLight3)
    const rectLightHelper3 = new RectAreaLightHelper(rectLight3);
    rectLight3.add(rectLightHelper3);
    const rectLight4 = new THREE.RectAreaLight(color, intensity, width, height);
    rectLight4.position.set(10, -8, 0);
    rectLight4.lookAt(10, 0, 0);
    scene.add(rectLight4)
    const rectLightHelper4 = new RectAreaLightHelper(rectLight4);
    rectLight4.add(rectLightHelper4);
    //gauche
    const rectLight5 = new THREE.RectAreaLight(color, intensity, width, height);
    rectLight5.position.set(-10, 15, 0);
    rectLight5.lookAt(-10, 0, 0);
    scene.add(rectLight5)
    const rectLightHelper5 = new RectAreaLightHelper(rectLight5);
    rectLight5.add(rectLightHelper5);
    const rectLight6 = new THREE.RectAreaLight(color, intensity, width, height);
    rectLight6.position.set(-10, -8, 0);
    rectLight6.lookAt(-10, 0, 0);
    scene.add(rectLight6)
    const rectLightHelper6 = new RectAreaLightHelper(rectLight6);
    rectLight6.add(rectLightHelper6);


    


    function createDESC(title, desc, id) {
      if (document.getElementById("divText") != null) {
        document.getElementById("divText").remove()
        document.getElementById("btnBack").style.display = "none"
        document.getElementById("divPortfolio").remove()
      }
      var divText = document.createElement("div")
      divText.setAttribute("id", "divText")
      var pTitle = document.createElement("p")
      pTitle.setAttribute("id", "pTitle")
      pTitle.innerHTML = title
      var pDesc = document.createElement('p')
      pDesc.setAttribute("id", "pDesc")
      pDesc.innerHTML = desc
      var btnStart = document.createElement('button')
      btnStart.setAttribute("id", "btnStart")
      el = btnStart
      fx = new TextScramble(el)
      fx.setText("LANCER LE PROGRAMME")
      container.appendChild(divText)
      divText.appendChild(pTitle)
      divText.appendChild(pDesc)
      divText.appendChild(btnStart)
      //photoPortfolio
      var divPortfolio =  document.createElement("div")
      divPortfolio.setAttribute("id", "divPortfolio")
      var line = document.createElement("div")
      line.setAttribute("id", "line")
      divPortfolio.appendChild(line)
      var carrousselImage = document.createElement("div")
      carrousselImage.setAttribute("id", "carrousselImage")
      divPortfolio.appendChild(carrousselImage)
      var img = document.createElement("img")
      for (let i = 0; i < nbImg; i++){
        img.src = imagesTab[id][i]
        img.setAttribute("id", "img")
        carrousselImage.appendChild(img)
        img = document.createElement("img")
      }
      container.appendChild(divPortfolio)
      //btn retour
      var btnBack =document.getElementById("btnBack")
      btnBack.style.display = "block"
      // var iconArrow = document.getElementById("iconArrow")
      btnBack.addEventListener("click", onbtnBackClick)
      //click
      var url = urlsTab[id]
      btnStart.addEventListener('click', function(){onbtnStartClick(url)}, false)
    }

    

    function onbtnBackClick() {
      document.getElementById("divText").remove()
      document.getElementById("btnBack").style.display = "none"
      document.getElementById("divPortfolio").remove()
      zoomOut()
    }

    function onbtnStartClick(url) {
      var divBgBlack = document.createElement("div")
      divBgBlack.setAttribute("id", "divBgBlack")
      var divProgressBar = document.createElement("div")
      divProgressBar.setAttribute("id", "divProgressBar")
      var divProgressBarValue = document.createElement("div")
      divProgressBarValue.setAttribute("id", "divProgressBarValue")
      var pProgressBar = document.createElement("p")
      pProgressBar.setAttribute("id", "pProgressBar")
      pProgressBar.innerHTML = "INITIALISATION..."
      container.appendChild(divBgBlack)
      container.appendChild(divProgressBar)
      divProgressBar.appendChild(divProgressBarValue)
      divProgressBarValue.appendChild(pProgressBar)
      setTimeout(function(){
        window.open(url, "_blank")
        divBgBlack.remove()
        divProgressBar.remove()
      }, 3000);
    }

    var controls = new OrbitControls( camera, renderer.domElement );
    controls.enabled = false;

    function zoomOut(){
      controls.enabled = true;
      gsap.to( camera, {
				duration: 1.5,
        zoom: 1,
				onUpdate: function () {
          
					camera.updateProjectionMatrix();
          camera.position.y = 1
				}
			} );
			
			gsap.to( controls.target, {
				duration: 2,
				x: 0,
				y: 0,
				z: 0,
				onUpdate: function () {
				
					controls.update();
				
				}
      } );
      controls.enabled = false;
      positionAnimCheck = true;
    }


    function zoomOnObject(object){
      positionAnimCheck = false
      controls.enabled = true;
			gsap.to( camera, {
				duration: 1.5,
        zoom: 7,
				onUpdate: function () {
          
					camera.updateProjectionMatrix();
          camera.position.y = object.position.y
				}
			} );
			
			gsap.to( controls.target, {
				duration: 1,
				x: object.position.x,
				y: object.position.y,
				z: object.position.z,
				onUpdate: function () {
				
					controls.update();
				
				}
      } );
      controls.enabled = false;
    }
    var positionAnimCheck = true

    //mouse click

    var mouse = new THREE.Vector2();
    var raycaster = new THREE.Raycaster();
    labelRenderer.domElement.addEventListener('click', onClick, false);
    function onClick(event) {
      event.preventDefault();

      mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
      mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

      raycaster.setFromCamera(mouse, camera);

      var intersects = raycaster.intersectObject(scene, true);

      if (intersects.length > 0 ) {
      
        var object = intersects[0].object;
        
        if (object.id> 10 && object.id < 16) {
          
          focusProject(object)

        }
      }

      labelRenderer.render(scene, camera);
    }

    function animate() {
      
      requestAnimationFrame(animate);
      rotationAnim()
      labelRenderer.render( scene, camera );
      renderer.render(scene, camera);
      

    }
    var u = 0

    function rotationAnim() {

      var rotate_val = [0.01, 0.014, 0.01, 0.015, 0.01, 0.01, 0.013, 0.01, 0.012, 0.01]

      var i = 0
      for (let object of objects) {

        object.rotation.z += rotate_val[i];
        object.rotation.x += rotate_val[i + 1];
        if (positionAnimCheck == true) {
          object.position.x = Math.cos(t_val[i] + u) * 3;
          object.position.z = Math.sin(t_val[i] + u) * 3;
          texts[i].position.x = object.position.x
          texts[i].position.z = object.position.z 
          // textObjects[i].position.set(object.position.x+0.75, object.position.y+0.5, object.position.z)
        }
        i += 1
      }
      u += speed


    }


    function focusProject(project){
        
      if(project.position.z < 0){
        speed = 0.08
        setTimeout(function() {
          speed = 0.008
          zoomOnObject(project)
          setTimeout(function() {
            createDESC(titleTab[project.id - 11], descTab[project.id - 11], (project.id - 11))
            }, 1000)
        }, 700);

      }
      else{
        zoomOnObject(project)
        setTimeout(function() {
        createDESC(titleTab[project.id - 11], descTab[project.id - 11], (project.id - 11))
        }, 1000)
      }
    
      
    }
    function onDocumentMouseDown(event) {

      event.preventDefault();
      console.log(event);
    }
    animate();
  }
  }
  render() {
    const { width } = this.state;
    if (width <= 500) {
    return (
      <div>
        <p>Mobile</p>

      </div>
    );
    }
    else {
      return(
      <div>
      <div id="container">
        <button id="btnBack">
          <span id="iconArrow"><FontAwesomeIcon icon={faArrowLeft} /></span>
        </button>  
      </div>
      
    </div>
      );
    }
  }
}

export default App;